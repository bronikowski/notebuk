import fpdf

class LinesFooter(fpdf.FPDF):
    def footer(self):
        self.set_y(-10)
        self.set_font('Arial', 'I', 8)
        self.cell(0, 10, '%s' % self.page_no(), 0, 0, 'C')

class Pages(object):

    def __init__(self, name, *args, **kwargs):

        self.name = name

        self.page_format = kwargs.pop('format', 'A5')
        self.line_margins = kwargs.pop('line_margins', 5)
        self.color = kwargs.pop('color', 200)
        self.line_width = kwargs.pop('line_width', .1)
        self.density = kwargs.pop('density', 9)

    def render(self, renderer=fpdf.FPDF, number_of_pages=1):

        pdf = renderer(format=self.page_format)
        pdf.add_page()

        pages_rendered = 0

        while True:
            
            if pages_rendered == number_of_pages:
                break

            #print("{} @ {}".format(pdf.page_no(), pdf.get_y()))
            pdf.set_draw_color(self.color, self.color, self.color) # greyscale
            pdf.set_line_width(self.line_width)

            if pdf.get_y() + self.density > pdf.fh:
                pages_rendered += 1
                if pages_rendered == number_of_pages:
                    break
                pdf.add_page()
                #print("BREAK")
                continue

            pdf.line(self.line_margins, pdf.get_y(), pdf.fw - self.line_margins, pdf.get_y())
            pdf.set_y(pdf.get_y() + self.density)

        pdf.output(self.name, dest='F')

