# pages.Pages

I tried to use Scribus to create a ruled pages for my DIY notepads but me + mices = tragedy. So I did what any sane person do: I wrote some Python that throws PDFs at me.

## Usage


```
#!python

from pages import Pages

your_file = Pages('aintitfun.pdf')
your_file.render()
```

## Configuration

You can pass kwargs to constructor. Currently supported items:


```
#!python

format # 'A4', 'A5', 'letter' or a touple 
line_margins # how much space to leave 
color # 0…255, I only do greyscale
density # space between lines
line_width # line width?
```

i.e.


```
#!python

a4_with_black_lines = {'format': 'A4', 'color': 0}
your_file = Pages('itsdifferentnow.pdf', **a4_with_black_lines)

```


You can provide custom renderer (a class that inherits from FPDF) and a number of pages to renderer:


```
#!python

from pages import LinesFooter
your_file.render(renderer=LinesFooter, number_of_pages=5)
```

![default.png](https://bitbucket.org/repo/Ao8qeb/images/2005976706-default.png)