import pages

default = pages.Pages('default.pdf')
default.render()

tight = {'density': 6}
black = {'color': 0}
custom_size = {'page_format': (100,100)}
todo = {'density': 20, 'line_margins': 30, 'format': (120, 280)}

tight = pages.Pages('tight.pdf', **tight)
tight.render()

todo = pages.Pages('todo.pdf', **todo)
todo.render()

custom_size = pages.Pages('custom_size.pdf', **custom_size)
custom_size.render()

black = pages.Pages('black.pdf', **black)
black.render(renderer=pages.LinesFooter, number_of_pages=10)

